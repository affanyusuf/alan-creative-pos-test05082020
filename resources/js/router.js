import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

import PosIndex from './pages/pos/index'

const router = new VueRouter({
    routes: [
        {
            path: '/',
            name: 'home',
            component: PosIndex
        }
    ]
})

export default router;

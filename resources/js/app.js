require('./bootstrap');

import Vue from 'vue'
import ElementUI from 'element-ui';
import locale from 'element-ui/lib/locale/lang/en'
import App from './App.vue';
import router from './router'

Vue.use(ElementUI, { locale })

const app = new Vue({
    el: '#app',
    router,
    render: h => h(App)
});
